﻿using System;
using NaughtyAttributes;
using UnityEngine;

namespace KubirillBoss
{
    public class WaveSpawner : MonoBehaviour
    {
        public float WaveSpeed = 20;
        public float LifeTime = 20;
        public float WaveStrength = 80;
        public WavesInAtake[] smallWave;
        private int _waveNumberAtake;
        public Wave Wall;
        
        public float waveStep;
        public Transform waveExample;
        public Transform waveSpawner;
        public Transform defenseZone;
        private bool _isSpawn;
        private float _nextSpawn;
        private void Update()
        {
            if (_isSpawn)
            {
                if (-GetLocalXCoordinate()>_nextSpawn)
                {
                    SpawnWave();
                    _nextSpawn = _nextSpawn + waveStep;
                }
            }
        }
        [Button()]
        public float GetLocalXCoordinate()
        {
            var relativePosition = waveSpawner.position - waveExample.position;
            Vector3 localProjection = waveExample.InverseTransformDirection(relativePosition);
            return localProjection.x;
        }

        void SpawnWave()
        {
            var index = (int)(_nextSpawn / waveStep);
            index = index % smallWave[_waveNumberAtake].Waves.Length;
            Wave wave;
            if ((int)(_nextSpawn / waveStep) < 5)
            {
                wave = Instantiate(Wall, waveSpawner.position, waveExample.rotation,waveExample);
            }

            else
            {
                wave = Instantiate(smallWave[_waveNumberAtake].Waves[index], waveSpawner.position, waveExample.rotation,waveExample);
            }
            
            wave.transform.localPosition = new Vector3(-_nextSpawn, 0, wave.transform.localPosition.z);
            wave.transform.localScale = Vector3.one;
            wave.speed = WaveSpeed;
            wave.lifeTime = LifeTime;
            wave.strength = WaveStrength;
        }

        public void StartAtake()
        {
            
            smallWave[_waveNumberAtake].ShuffleWaves();
            _isSpawn = true;
            
        }

        public void StopAtake()
        {
            _isSpawn = false;
            _nextSpawn = 0;
            _waveNumberAtake = (_waveNumberAtake + 1) % smallWave.Length;
        }
        private void OnDisable()
        {
            Destroy(defenseZone.gameObject);
        }
        
    }
}