﻿using System;
using UnityEngine;

namespace KubirillBoss
{
    public class Wave : MonoBehaviour
    {
        public float speed=10;
        public float lifeTime=10;
        public float strength = 300;
        private void Start()
        {
           if (lifeTime>0) Invoke("DestroyWave",lifeTime);
        }

        private void Update()
        {
            transform.Translate(transform.forward * (speed * Time.deltaTime),Space.World);
        }

        void DestroyWave()
        {
            Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (lifeTime>0)  GetComponent<Collider>().enabled=false;
                Vector3 throwVector = transform.forward +Vector3.up/2;
                collision.gameObject.GetComponent<Rigidbody>().velocity = throwVector* strength;
            }
        }

       
    }
}