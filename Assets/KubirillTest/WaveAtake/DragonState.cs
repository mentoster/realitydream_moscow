﻿using System;
using UnityEngine;

namespace KubirillBoss
{
    public class DragonState : MonoBehaviour
    {
        public MonoBehaviour[] Behaviours;
        public int current = 0;
        private Animator _anim;

        private void Start()
        {
            _anim = GetComponent<Animator>();
        }

        public void DissableCurrent()
        {
            Behaviours[current].enabled = false;
            current++;
            
        }

        public void SetDamage()
        {
            _anim.SetTrigger("Damage");
        }

        public void BeginNext()
        {
            
            
            if (current<Behaviours.Length) Behaviours[current].enabled = true;
            _anim.SetTrigger("BeginFirstAtake");
        }
    }
}