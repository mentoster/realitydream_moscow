﻿using UnityEngine;

namespace KubirillBoss
{
    [CreateAssetMenu(fileName = "WavesAtake", menuName = "WaveBoss", order = 0)]
    public class WavesInAtake : ScriptableObject
    {
        [field: SerializeField] public Wave[] Waves;


        public void ShuffleWaves()
        {
            ShuffleArray(Waves);
        }
         
        
        public void ShuffleArray<T>(T[] array)
        {
            System.Random rng = new System.Random();

            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n);
                n--;
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }



    }
}