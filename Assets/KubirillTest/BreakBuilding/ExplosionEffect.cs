﻿using UnityEngine;

namespace KubirillBoss
{
    public class ExplosionEffect : MonoBehaviour
    {
        public GameObject particles;
        private bool _isActive;
        public Transform joint;
        public float period = 0.2f;
        public void BeginSpawn()
        {
            _isActive = true;
            CreateParticles();
        }
        public void StopSpawn()
        {
            _isActive = false;
        }

        void CreateParticles()
        {
            var ecpl = Instantiate(particles, joint);
            if (_isActive) Invoke("CreateParticles",period);
        }
    }
}