﻿using System;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace KubirillBoss
{
    public class BuildingWeakPoint : MonoBehaviour
    {
        public int hp = 30;
        public MeshRenderer MeshRendererl;
        public Action DestroyBuilding;
        public GameObject explosionEffect;
        
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Damage")
            {
                hp--;
                MeshRendererl.material.color=Color.red;
                Invoke("RestoreColor",0.1f);
                if (hp<=0)DestriyComplite();
            }
        }

        void DestriyComplite()
        {
            DestroyBuilding?.Invoke();
            for (int i=0; i<10; i++)
            {
                var expl=Instantiate(explosionEffect, transform.position,Quaternion.identity);
                expl.transform.position +=
                    new Vector3(Random.Range(-20, 20), Random.Range(-50, 50), Random.Range(-20, 20));
                expl.transform.localScale= Vector3.one*100;
            }
            Destroy(gameObject);
        }
        void RestoreColor()
        {
            MeshRendererl.material.color=Color.yellow;
        }

        
    }
}