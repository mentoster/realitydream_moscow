﻿using System;
using UnityEngine;

namespace KubirillBoss
{
    public class BuildingFall : MonoBehaviour
    {
        public BuildingWeakPoint _buildingWeakPoint;
        private Animator _anim;
        public DragonState DragonState;
        private void Start()
        {
            if (DragonState == null)
            {
                DragonState = FindObjectOfType<DragonState>();
            }
            if (_buildingWeakPoint==null)
            {
                _buildingWeakPoint = GetComponent<BuildingWeakPoint>();
            }

            _buildingWeakPoint.DestroyBuilding += StartDestroy;
            _anim=GetComponent<Animator>();
        }

        private void StartDestroy()
        {
            _anim.SetTrigger("Fall");
        }
        private void FallComplite()
        {
            DragonState.DissableCurrent();
            DragonState.SetDamage();
        }
    }
}