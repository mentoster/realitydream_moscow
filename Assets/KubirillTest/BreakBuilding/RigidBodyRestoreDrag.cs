using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidBodyRestoreDrag : MonoBehaviour
{
    private Rigidbody _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        _rb.drag = 0;
    }
}
