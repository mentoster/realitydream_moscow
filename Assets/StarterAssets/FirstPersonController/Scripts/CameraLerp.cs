using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLerp : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float lerpK = 0.1f;
    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, lerpK);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, lerpK);
    }
}
