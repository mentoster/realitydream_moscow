﻿using System;
using UnityEngine;

namespace StarterAssets
{
    public class SpearThrowing : MonoBehaviour
    {
        [Header("Spear Throwing")]
        [SerializeField] private GameObject spearRef;
        [SerializeField] private float spearThrowForce;
        [SerializeField] private Transform spearThrowingPoint;
        [SerializeField] private Transform cameraRoot;

        private void Update()
        {
            Shoot();
        }

        void Shoot()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var spear = Instantiate(spearRef, spearThrowingPoint.position, cameraRoot.rotation);
                spear.GetComponentInChildren<Rigidbody>().AddForce(cameraRoot.forward * spearThrowForce);
            }
        }
    }
}
