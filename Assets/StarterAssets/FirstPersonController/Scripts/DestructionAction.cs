﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class DestructionAction
{
    public Rigidbody target;
    public Vector3 force;
    public Vector3 torque;
    public float delay;
    public bool useLocalSpace;

    public void Activate()
    {
        
    }
    
    
}