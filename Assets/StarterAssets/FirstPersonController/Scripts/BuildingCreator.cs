using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCreator : MonoBehaviour
{
    public int blockNumberX;
    public int blockNumberY;
    public int blockNumberZ;
    public float blockHeight;
    public float blockWidth;
    public float blockDepth;
    public GameObject block;
    

    public void Build()
    {
        var building = new GameObject("Building");
        building.transform.parent = transform;
        building.transform.localPosition = Vector3.zero;
        building.transform.localRotation = Quaternion.identity;
        
        Vector3 initialPos = transform.position;
        Vector3 curentPos = Vector3.zero;
        for (var x = 0; x < blockNumberX; x++)
        {
            for (var y = 0; y < blockNumberY; y++)
            {
                for (var z = 0; z < blockNumberZ; z++)
                {
                    var instantiate = Instantiate(block, initialPos, transform.rotation, building.transform);
                    instantiate.transform.localPosition = curentPos;
                    curentPos.z += blockDepth * block.transform.localScale.z;
                }

                curentPos.z = 0;
                curentPos.y += blockHeight* block.transform.localScale.y;
            }

            curentPos.y = 0;
            curentPos.x += blockWidth* block.transform.localScale.x;
        }
        
    }
}
