using System;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;

public class Spear : MonoBehaviour
{
    [HideInInspector] public Rigidbody playerRb;
    private Rigidbody rb;
    [SerializeField] private float lifetime = 10f;
    [SerializeField] private float instantMagneticForce;
    private bool isFlying = true;
    [SerializeField] GameObject particles;
    [Header("FMOD")]
    private FMOD.Studio.EventInstance _soundInstance;
    [SerializeField] private EventReference _airEvent;
    [SerializeField] private EventReference _groundEvent;


    void Start()
    {

        Destroy(gameObject, lifetime);
        rb = GetComponent<Rigidbody>();

        _soundInstance = FMODUnity.RuntimeManager.CreateInstance(_airEvent);
        Debug.Log(particles.transform.position);
        _soundInstance.set3DAttributes(particles.transform.position.To3DAttributes());
        _soundInstance.start();
    }

    void Update()
    {
        //transform.rotation *= Quaternion.FromToRotation(transform.forward, rb.velocity).normalized;
        //transform.Rotate(Vector3.forward);
        if (isFlying)
        {
            transform.rotation *= Quaternion.Euler(Vector3.SignedAngle(transform.forward, rb.velocity, transform.right), 0, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!isFlying)
        {
            return;
        }

        isFlying = false;

        particles.gameObject.SetActive(true);
        _soundInstance = FMODUnity.RuntimeManager.CreateInstance(_groundEvent);
        var soundPos = particles.transform.position + new Vector3(UnityEngine.Random.Range(-0.3f, 0.3f), 0, UnityEngine.Random.Range(-0.3f, 0.3f));
        _soundInstance.set3DAttributes(soundPos.To3DAttributes());
        _soundInstance.start();

        var otherRb = collision.gameObject.GetComponent<Rigidbody>();
        if (otherRb)
        {
            var fixedJoint = gameObject.AddComponent<FixedJoint>();
            fixedJoint.connectedBody = otherRb;

        }
        else
        {
            rb.isKinematic = true;



        }
    }

    public override bool Equals(object obj)
    {
        return obj is Spear spear &&
               base.Equals(obj) &&
               EqualityComparer<EventInstance>.Default.Equals(_soundInstance, spear._soundInstance);
    }
}
