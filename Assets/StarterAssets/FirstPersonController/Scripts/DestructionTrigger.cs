using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionTrigger : MonoBehaviour
{
    [SerializeField] private List<DestructionAction> actions;
    private bool wasTriggered = false;

    private void Start()
    {
        foreach (var action in actions)
        {
            action.target.isKinematic = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !wasTriggered)
        {
            foreach (var action in actions)
            {
                StartCoroutine("CauseDestruction", action);
            }
        }
    }

    IEnumerator CauseDestruction(DestructionAction action)
    {
        yield return new WaitForSeconds(action.delay);
        action.target.isKinematic = false;
        if(!action.useLocalSpace)
        {
            action.target.AddTorque(action.torque);
            action.target.AddForce(action.force);
        }
        else
        {
            action.target.AddTorque(action.target.transform.TransformDirection(action.torque));
            action.target.AddForce(action.target.transform.TransformDirection(action.force));
        }

       
    }
}
