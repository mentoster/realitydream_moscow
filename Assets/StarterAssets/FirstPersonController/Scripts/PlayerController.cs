using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using FMODUnity;
using StarterAssets;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] float forwardMoveSpeed;
    [SerializeField] private float sideMoveSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float maxSimpleVelocity;
    [SerializeField] private float maxVelocity;
    [SerializeField] private float sideFrictionForce;
    [SerializeField] private float speedAngle;
    [SerializeField] private float horizontalSpeedAngle;
    [SerializeField] private GameObject icePanel;

    [Header("Camera")]
    [SerializeField] private float cameraSkew = 7f;
    [SerializeField] private Transform cameraRoot;
    [SerializeField] private float cameraTurningSpeed;
    [SerializeField] private float cameraShaking;


    [Header("Ice Stamina")]
    [SerializeField] private float maxIceStamina = 100f;
    [SerializeField] private float iceStaminaConsumption = 2f;
    [SerializeField] private float iceStaminaRecovery = 2f;
    [SerializeField] private Transform feetPosition;
    [SerializeField] private float groundCheckRadius;


    private Rigidbody rb;
    CinemaCamera _cinemaCamera;
    IceSounds _iceSounds;
    private float iceStamina;
    private bool isCreatingTramplin = false;
    private bool canSpawnIce = true;
    private bool isGrounded;

    private bool _createIceFromJump = false;
    [Header("Ice Road")]
    [SerializeField] private float _iceDistance = 0.5f;
    [SerializeField] private float _iceRotation = 15f;
    [SerializeField] private float maxDistanceToGround;
    [SerializeField] private ParticleSystem _iceParticles;
    [Header("FMOD")]
    [SerializeField] EventReference _collectReference;
    [SerializeField] EventReference _groundReference;

    private FMOD.Studio.EventInstance _coinInstance;
    private FMOD.Studio.EventInstance _playerInstance;
    [Header("Effects")]
    [SerializeField] private Volume _volume;

    private Vector3 _oldSpawnPosition;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        _cinemaCamera = GetComponent<CinemaCamera>();
        _iceSounds = FindAnyObjectByType<IceSounds>();
        iceStamina = maxIceStamina;

        _coinInstance = FMODUnity.RuntimeManager.CreateInstance(_collectReference);
        _playerInstance = FMODUnity.RuntimeManager.CreateInstance(_groundReference);
    }

    private void Update()
    {
        Move();
    }



    private void Move()
    {
        CheckIfGrounded();
        if (isGrounded && !isCreatingTramplin)
        {
            CreateIceRoad();
            _iceSounds.PlaySound();
            _iceParticles.Play();
        }

        rb.AddForce(transform.right * (sideMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime));
        RotateCamera(Input.GetAxis("Horizontal") * -cameraSkew);

        if (Vector3.Project(rb.velocity, transform.forward).magnitude < maxSimpleVelocity)
        {
            rb.AddForce(transform.forward * (forwardMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime));
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            _createIceFromJump = true;
            rb.AddForce(jumpForce * Vector3.up);

        }

        if (Input.GetKey(KeyCode.LeftShift) && CheckIfCanCreateTramplin())
        {
            IceRoadMove();
            _iceSounds.PlaySound();
            _iceParticles.Play();
        }
        else
        {
            isCreatingTramplin = false;
            RecoverIceStamina();
            rb.useGravity = true;
            _iceSounds.StopSound();
            _iceParticles.Stop();

        }

        if (Input.GetKeyDown(KeyCode.LeftAlt) ||
            Input.GetKeyDown(KeyCode.RightAlt)) // Depending on which Alt key you want to use
        {
            var horizontalInput = Input.GetAxis("Horizontal");
            var verticalInput = Input.GetAxis("Vertical");
            Vector3 dashDirection;
        }

        ClampVelocity();
    }

    private void ClampVelocity()
    {
        if (rb.velocity.magnitude > maxVelocity)
        {
            rb.velocity = rb.velocity.normalized * maxVelocity;
        }
    }

    void CheckIfGrounded()
    {
        Collider[] colliders = Physics.OverlapSphere(feetPosition.position, groundCheckRadius);
        var numb = colliders.Length;
        // remove all colliders with tag damage
        for (var i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].CompareTag("Damage"))
            {
                numb--;
            }
        }
        isGrounded = numb > 1;
    }

    private bool isMovingForward()
    {
        return Vector3.Dot(transform.forward, rb.velocity) > 0;
    }

    bool CheckIfCanCreateTramplin()
    {
        if (iceStamina < 0 || !isMovingForward())
        {
            return false;
        }

        bool hitGround = Physics.Raycast(feetPosition.position, Vector3.down, maxDistanceToGround);
        var velocityY = Vector3.Dot(rb.velocity, Vector3.up) < 0;
        if (isGrounded || isCreatingTramplin || (hitGround && velocityY))
        {
            _createIceFromJump = false;
            return true;
        }

        return isGrounded || isCreatingTramplin;
    }

    void IceRoadMove()
    {
        iceStamina -= iceStaminaConsumption * Time.deltaTime;
        isCreatingTramplin = true;
        CreateIceRoad();
        rb.useGravity = false;
        if (Input.GetAxis("Horizontal") != 0 && isGrounded)
        {
            var forceDirection = -Vector3.Cross(rb.velocity, transform.up).normalized;
            rb.AddForce(forceDirection * (rb.velocity.magnitude * horizontalSpeedAngle * Time.deltaTime * Input.GetAxis("Horizontal")));
        }
        else
        {
            var forceDirection = Vector3.Cross(rb.velocity, transform.right).normalized;
            rb.AddForce(forceDirection * (rb.velocity.magnitude * speedAngle * Time.deltaTime));
        }
    }

    void RecoverIceStamina()
    {
        if (iceStamina >= maxIceStamina)
        {
            iceStamina = maxIceStamina;
            return;
        }

        iceStamina += iceStaminaRecovery * Time.deltaTime;
    }

    void ApplyHorizontalFriction()
    {
        if (Vector3.Project(rb.velocity, transform.right).magnitude > 0)
        {
            rb.AddForce(-Vector3.Project(rb.velocity, transform.right).normalized * (sideFrictionForce * Time.deltaTime));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            return;
        }
        var force = Mathf.Abs(rb.velocity.y * cameraShaking);

        _playerInstance.set3DAttributes(gameObject.To3DAttributes());
        _playerInstance.start();
        float volumeWeight = 0;
        if (force > 0.2)
        {
            volumeWeight = force * 3;
        }
        if (rb.velocity.y < -0.5f)
        {
            ApplyDamage(volumeWeight);
        }
        _cinemaCamera.GenerateImpulseOnLand(force);
    }

    public void ApplyDamage(float damage)
    {
        DOTween.To(() => _volume.weight, x => _volume.weight = x, damage, 0.1f).OnComplete(() => DOTween.To(() => _volume.weight, x => _volume.weight = x, 0, damage * 2.4f));
        _cinemaCamera.GenerateImpulseOnLand(damage);
    }

    private void CreateIceRoad()
    {
        // check distance between last ice and current position
        if (Vector3.Distance(_oldSpawnPosition, transform.position) < _iceDistance)
        {
            return;
        }

        var ice = Instantiate(icePanel, transform.position, quaternion.identity);

        // local scale
        ice.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        ice.transform.DOScale(Vector3.one * 2, 0.2f);

        // random rotate
        ice.transform.rotation = Quaternion.FromToRotation(transform.forward, rb.velocity);
        ice.transform.rotation *= Quaternion.Euler(UnityEngine.Random.Range(-_iceRotation, _iceRotation), UnityEngine.Random.Range(-_iceRotation, _iceRotation), UnityEngine.Random.Range(-_iceRotation, _iceRotation));

        // remember old position
        _oldSpawnPosition = ice.transform.position;

        var forceDirection = Vector3.Cross(rb.velocity, transform.right).normalized;
        ice.transform.position += -Vector3.up * 0.045f;
        ice.transform.position += rb.velocity.normalized;
    }

    private void RotateCamera(float targetRotationAngle)
    {
        var rotationAngle = cameraRoot.localEulerAngles.z;

        // Smoothly rotate the player towards the target rotation angle
        rotationAngle = Mathf.LerpAngle(rotationAngle, targetRotationAngle, cameraTurningSpeed * Time.deltaTime);

        cameraRoot.localRotation = Quaternion.Euler(cameraRoot.localEulerAngles.x, cameraRoot.localEulerAngles.y, rotationAngle);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            _coinInstance.set3DAttributes(other.gameObject.To3DAttributes());
            _coinInstance.start();
            Destroy(other.gameObject);
        }
    }
}
