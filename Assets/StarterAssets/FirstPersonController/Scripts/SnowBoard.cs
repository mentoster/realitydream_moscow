﻿using System;
using UnityEngine;

namespace StarterAssets
{
    public class SnowBoard: MonoBehaviour
    {
        private Rigidbody parentRb;
        private void Start()
        {
            parentRb = transform.parent.GetComponent<Rigidbody>();
        }

        private void Update()
        {
            
            Vector3 velocityForward = Vector3.ProjectOnPlane(parentRb.velocity, transform.right);
            Vector3 velocitySide = Vector3.ProjectOnPlane(parentRb.velocity, transform.up);
            if (velocityForward.magnitude == 0)
            {
                return;
            }
            transform.localRotation *= Quaternion.Euler(Vector3.SignedAngle(transform.forward, velocityForward,
                transform.right), Vector3.SignedAngle(transform.forward, velocitySide, Vector3.up), Vector3.SignedAngle(transform.right, parentRb.transform.right,
                parentRb.transform.forward));
        }
    }
}