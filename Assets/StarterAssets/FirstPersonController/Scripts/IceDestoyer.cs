using UnityEngine;

public class IceDestoyer : MonoBehaviour
{
    [SerializeField] private float lifetime = 6f;
    void Start()
    {
        Destroy(gameObject, lifetime);
    }
    
}
