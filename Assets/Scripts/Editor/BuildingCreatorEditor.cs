using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BuildingCreator))]
public class BuildingCreatorEditor : Editor
{
    private SerializedProperty _mConfiguration;
    private void OnEnable()
    {
        _mConfiguration = serializedObject.FindProperty("block");
    }

    public override void OnInspectorGUI()
    {
        BuildingCreator creator = (BuildingCreator)target;
        creator.blockNumberX = EditorGUILayout.IntField("blocks number X", creator.blockNumberX);
        creator.blockNumberY = EditorGUILayout.IntField("blocks number Y", creator.blockNumberY);
        creator.blockNumberZ = EditorGUILayout.IntField("blocks number Z", creator.blockNumberZ);

        creator.blockWidth = EditorGUILayout.FloatField("block width", creator.blockWidth);
        creator.blockHeight = EditorGUILayout.FloatField("block height", creator.blockHeight);
        creator.blockDepth = EditorGUILayout.FloatField("block depth", creator.blockDepth);
        
        EditorGUILayout.PropertyField(_mConfiguration, new GUIContent("block"));

        if (GUILayout.Button("Build"))
        {
            creator.Build();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
