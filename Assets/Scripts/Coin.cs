using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

using UnityEngine;
public class Coin : MonoBehaviour
{
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        SetRandomSpeed();
    }

    private void SetRandomSpeed()
    {
        float randomSpeed = Random.Range(0.5f, 1.5f);
        _animator.speed = randomSpeed;
    }
}
