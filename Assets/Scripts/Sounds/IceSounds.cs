using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class IceSounds : MonoBehaviour
{
    [SerializeField] private EventReference _createIce;
    private FMOD.Studio.EventInstance _eventInstance;
    private bool _isPlaying = false;

    public void Start()
    {
        _eventInstance = FMODUnity.RuntimeManager.CreateInstance(_createIce);
        _eventInstance.set3DAttributes(RuntimeUtils.To3DAttributes(gameObject));
    }
    public void PlaySound()
    {
        if (!_isPlaying)
        {
            _eventInstance.start();
            _isPlaying = true;
        }
    }
    public void StopSound()
    {
        if (_isPlaying)
        {
            _isPlaying = false;
            _eventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
}
