using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public EventReference eventReference;

    private FMOD.Studio.EventInstance _musicInstance;

    private void OnEnable()
    {
        _musicInstance = FMODUnity.RuntimeManager.CreateInstance(eventReference);
        _musicInstance.setParameterByName("Looping", 1);
        _musicInstance.start();
    }

    private void OnDestroy()
    {
        _musicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _musicInstance.release();
    }
}
