using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private ParticleSystem _moveParticles;
    [SerializeField] private GameObject _hitParticles;
    [SerializeField] private MeshRenderer _meshRenderer;
    private Rigidbody _rb;

    [SerializeField] private EventReference _flyReference;
    [SerializeField] private EventReference _boomReference;
    [SerializeField] private float _explosionForce = 10.0f;
    [SerializeField] private float _explosionRadius = 5.0f;
    public bool isBoomed = false;
    private FMOD.Studio.EventInstance _ballInstance;

    private void Start()
    {
        _ballInstance = FMODUnity.RuntimeManager.CreateInstance(_flyReference);
        _ballInstance.start();
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(_ballInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        _rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!isBoomed)
        {
            isBoomed = true;
        }
        else
        {
            return;
        }
        // On collision, hide the mesh renderer, stop the moving particles, and start the hit particles
        _ballInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _ballInstance = FMODUnity.RuntimeManager.CreateInstance(_boomReference);
        _ballInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        _ballInstance.start();
        // set sound position
        _rb.isKinematic = true;

        var colliders = Physics.OverlapSphere(transform.position, _explosionRadius);
        Debug.Log("Detected " + colliders.Length + " colliders.");

        foreach (var hit in colliders)
        {
            Debug.Log("Hit object: " + hit.gameObject.name);
            if (hit.gameObject.CompareTag("Player"))
            {
                // Add an explosion force to the player
                Debug.Log("Player hit");
                hit.gameObject.transform.parent.GetComponent<Rigidbody>().AddExplosionForce(_explosionForce, transform.position, _explosionRadius);

                var distance = Vector3.Distance(hit.gameObject.transform.position, transform.position);
                var damage = 1 - (distance / _explosionRadius);
                hit.gameObject.transform.parent.GetComponent<PlayerController>().ApplyDamage(damage);
            }
        }

        _ballInstance.start();
        _meshRenderer.enabled = false;
        _moveParticles?.Stop();
        _hitParticles.SetActive(true);
        GetComponent<Collider>().enabled = false;

        // Destroy the fireball after 3 seconds
        Destroy(gameObject, 3f);
    }
}
