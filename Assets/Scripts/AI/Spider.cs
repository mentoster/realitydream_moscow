using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class Spider : MonoBehaviour
{
    private Animator _animator; // Animator component attached to the AI character
    [SerializeField] private string _deathAnimationName = "Die"; // Name of the death animation
    private Rigidbody _rb;
    private bool _isDead = false;
    [SerializeField] private GameObject _fireballPrefab;  // Prefab for the fireball
    [SerializeField] private float _fireballSpeed = 10f;  // Speed at which the fireball moves
    [SerializeField] private Transform _firePosition;  // Position from which the fireball is spawned
    [SerializeField] private float _attackInterval = 2.5f;  // Interval between attacks
    [SerializeField] private float _attackRange = 50f;
    private GameObject _player;  // Reference to the player
    private Rigidbody _playerRb;
    private float _lastAttackTime;  // Time of the last attack
    [SerializeField] private float _fireballHeight = 10f;  // Height of the parabola
    [SerializeField] private float _rotationTime = 1;

    private FMOD.Studio.EventInstance _spiderInstance;
    [SerializeField] private EventReference _fireballInstance;
    [SerializeField] private EventReference _dieInstance;


    private void Start()
    {
        _spiderInstance = FMODUnity.RuntimeManager.CreateInstance(_fireballInstance);


        _animator = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody>();

        _player = GameObject.FindWithTag("Player");
        _playerRb = _player.GetComponent<Rigidbody>();
    }
    private void OnDestroy()
    {
        _spiderInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _spiderInstance.release();
    }
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("Spider hit");
        if (other.gameObject.CompareTag("Damage"))
        {
            Debug.Log("Spider hit2");
            _rb.isKinematic = true;
            PlayDeathAnimation();
        }
    }
    private void FixedUpdate()
    {

        // If the player is within attack range and it's been long enough since the last attack, attack again
        if (!_isDead && Vector3.Distance(transform.position, _player.transform.position) <= _attackRange)
        {
            RotateTowardsPlayer();
            if (Time.time >= _lastAttackTime + _attackInterval)
            {
                StartCoroutine(AttackPlayer());
                _lastAttackTime = Time.time;
            }
        }

    }

    private void PlayDeathAnimation()
    {
        if (_isDead)
        {
            return;
        }
        _isDead = true;
        _animator.SetTrigger(_deathAnimationName);

        _spiderInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _spiderInstance = FMODUnity.RuntimeManager.CreateInstance(_dieInstance);
        _spiderInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        _spiderInstance.start();
    }
    private void RotateTowardsPlayer()
    {
        // Get the direction to the player
        Vector3 directionToPlayer = _player.transform.position - transform.position;
        directionToPlayer.y = 0;  // This line can ensure the spider only rotates around the Y axis

        // Calculate the rotation needed to face the player
        Quaternion targetRotation = Quaternion.LookRotation(directionToPlayer);

        // Interpolate from the spider's current rotation to the target rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * _rotationTime);
    }

    // Call this method when you want the spider to attack
    private IEnumerator AttackPlayer()
    {
        _animator.SetTrigger("Cast Spell");

        // set position for sound
        _spiderInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        _spiderInstance.start();

        yield return new WaitForSeconds(0.5f);


        // Predict the player's position when the fireball reaches them
        var distanceToPlayer = Vector3.Distance(_player.transform.position, _firePosition.position);
        var _closeDistanceThreshold = 12;
        Vector3 predictedPosition;
        float timeToPlayer;
        // If the player is very close, aim directly at their current position
        if (distanceToPlayer < _closeDistanceThreshold)
        {
            predictedPosition = _player.transform.position;
            timeToPlayer = 1;
        }
        else
        {
            // Predict the player's position when the fireball reaches them
            timeToPlayer = distanceToPlayer / _fireballSpeed;
            predictedPosition = _player.transform.position + _playerRb.velocity * timeToPlayer;
        }

        // Instantiate the fireball and get its Rigidbody
        GameObject fireball = Instantiate(_fireballPrefab, _firePosition.position, Quaternion.identity);
        Rigidbody fireballRb = fireball.GetComponent<Rigidbody>();

        // Start the coroutine to move the fireball
        StartCoroutine(MoveFireball(fireball, predictedPosition, timeToPlayer));
    }
    private IEnumerator MoveFireball(GameObject fireball, Vector3 target, float duration)
    {
        float time = 0;
        Vector3 start = fireball.transform.position;
        Fireball fr = fireball.GetComponent<Fireball>();
        while (time <= duration)
        {
            if (fr.isBoomed)
            {
                break;
            }
            float t = time / duration;

            // Calculate the next position
            Vector3 next = (1 - t) * start + t * target;

            next.y += _fireballHeight * (1 - 4 * (t - 0.5f) * (t - 0.5f));  // Parabolic function

            // Move the fireball to the next position
            fireball.transform.position = next;

            time += Time.deltaTime;
            yield return null;
        }

        if (!fr.isBoomed)
        {
            fireball.transform.position = target;
        }
    }
}
